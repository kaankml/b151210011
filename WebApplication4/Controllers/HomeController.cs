﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using WebApplication4.Models;
namespace WebApplication4.Controllers
{
    public class HomeController : Controller
    {
        VeritabaniEntities db = new VeritabaniEntities();

        public ActionResult Index()
        {
            var obj = db.anasayfas.ToList();   
            return View(obj);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            var obj = db.beslenmes.ToList();
            return View(obj);
        }

        // GET: iletisims/Create
        public ActionResult Contact()
        {
            return View();
        }

        // POST: iletisims/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact([Bind(Include = "id,email,mesaj")] iletisim iletisim)
        {
            if (ModelState.IsValid)
            {
                db.iletisims.Add(iletisim);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(iletisim);
        }


        public ActionResult Egzersiz()
        {
            var obj = db.egzersizs.ToList();
            return View(obj);
        }
    }
}